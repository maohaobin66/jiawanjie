import Vue from 'vue'
import Router from 'vue-router'
import Footer from '@/components/footer'
import index from '@/components/index'
// import Signup from '@/components/signup'
import shopList from '@/components/shopList'
import cart from '@/components/cart'
import Nshops from '@/components/Nshops'
import mine from '@/components/mine'
import LookMap from '@/components/LookMap'
// import NotFound from '@/components/notfound'
import shopDtail from '@/components/shopDtail'
import map from '@/components/map'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Footer',
      component: Footer,
      children:[
      {
      	path:"footer/index",
      	name:'index',
      	component:index
      },
      {
      	path:"footer/shopList",
      	name:"shopList",
      	component:shopList
      },
      {
      	path:"footer/cart",
      	name:"cart",
      	component:cart
      },
      {
      	path:"footer/Nshops",
      	name:"Nshops",
      	component:Nshops
      },
      {
      	path:"footer/mine",
      	name:"mine",
      	component:mine
      },
      {
      	path:"footer/LookMap",
      	name:"LookMap",
      	component:LookMap
      }
      ]
    },

  ]
})
